package com.fabiankranewitter.mcss.api;

public class McColorConverter {
	
	public McColorConverter() {}
	
	private HtmlTag convertMctoHexColor(char mcColor) {
		switch (mcColor) {
			case '0': return new HtmlTag("<font color=#000000>","</font>");
			case '1': return new HtmlTag("<font color=#0000AA>","</font>");
			case '2': return new HtmlTag("<font color=#00AA00>","</font>");
			case '3': return new HtmlTag("<font color=#00AAAA>","</font>");
			case '4': return new HtmlTag("<font color=#AA0000>","</font>");
			case '5': return new HtmlTag("<font color=#AA00AA>","</font>");
			case '6': return new HtmlTag("<font color=#FFAA00>","</font>");
			case '7': return new HtmlTag("<font color=#AAAAAA>","</font>");
			case '8': return new HtmlTag("<font color=#555555>","</font>");
			case '9': return new HtmlTag("<font color=#5555FF>","</font>");
			case 'a': return new HtmlTag("<font color=#55FF55>","</font>");
			case 'b': return new HtmlTag("<font color=#55FFFF>","</font>");
			case 'c': return new HtmlTag("<font color=#FF5555>","</font>");
			case 'd': return new HtmlTag("<font color=#FF55FF>","</font>");
			case 'e': return new HtmlTag("<font color=#FFFF55>","</font>");
			case 'f': return new HtmlTag("<font color=#D7D5D5>","</font>");
			default:
				return null;
			}
	}


	public HtmlTag convertMctoHtmlTags(char mctag) {
		switch (mctag) {
			case 'l': return new HtmlTag("<strong>","</strong>");
			case 'm': return new HtmlTag("<s>","</s>");
			case 'n': return new HtmlTag("<u>","</u>");
			case 'o': return new HtmlTag("<i>","</i>");
			case 'k': return new HtmlTag("<a class='obfuscate'>","</a>");
			default:
				return null;
			}
	}

	public String convertMotdtoHtml(String motd) {
		String tmpHtmlCode = "";
		HtmlTag currentFormTag = null;
		HtmlTag currentColorTag = null;
		
		//replace \n with html <br>
		motd = motd.replaceAll("\n", "<br>");

		String[] motdSections = motd.split("§");
		for (String motdSection : motdSections) {
			
			if (motdSection.length() == 0) continue;
			boolean mcCodeUsed = false;
			char mcCode = motdSection.charAt(0);
			
			//reset form and color
			if(mcCode == 'r') {
				if(currentFormTag!=null) {
					tmpHtmlCode += currentFormTag.stopTag;
					currentFormTag = null;
				}
				if(currentColorTag!=null) {
					tmpHtmlCode += currentColorTag.stopTag;
					currentColorTag = null;
				}
				mcCodeUsed = true;
			}
			
			//check form tag
			HtmlTag formTag = convertMctoHtmlTags(mcCode);
			if(formTag!=null) {
				if(currentFormTag!=null) {
					tmpHtmlCode += currentFormTag.stopTag;
				}
				currentFormTag = formTag;
				tmpHtmlCode += currentFormTag.startTag;
				mcCodeUsed = true;
			}
			
			//check color tag
			HtmlTag colorTag = convertMctoHexColor(mcCode);
			if(colorTag!=null) {
				if(currentColorTag!=null) {
					tmpHtmlCode += currentColorTag.stopTag;
				}
				currentColorTag = colorTag;
				tmpHtmlCode += currentColorTag.startTag;
				mcCodeUsed = true;
			}
			
			if(!mcCodeUsed) {
				tmpHtmlCode += "§"+mcCode;
			}
			
			//add text
			String text = motdSection.substring(1);
			tmpHtmlCode += text;
		}
		return tmpHtmlCode;
	}
	
	class HtmlTag {

		public final String startTag;
		public final String stopTag;

		public HtmlTag(String startTag, String stopTag) {
			this.startTag = startTag;
			this.stopTag = stopTag;
		}
	}

}