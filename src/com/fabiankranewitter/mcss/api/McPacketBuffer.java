package com.fabiankranewitter.mcss.api;

import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;


/*
 * https://wiki.vg/Protocol#Data_types
 */
public class McPacketBuffer{
	
	private DataOutputStream dataOutputStream;
	private ByteArrayOutputStream byteArrayOutputStream;

	
	public McPacketBuffer(int packetId) throws IOException {
		this();
		writeByte(packetId);
	}
	
	public McPacketBuffer() {
		byteArrayOutputStream = new ByteArrayOutputStream();
		dataOutputStream = new DataOutputStream(byteArrayOutputStream);
	}

	public void writeString(String text) throws IOException {
		writeVarInt(text.length());
		this.dataOutputStream.writeBytes(text);
	}

	private void writeVarInt(DataOutputStream dataOutputStream, int varInt) throws IOException {
		while (true) {
			if ((varInt & 0xFFFFFF80) == 0) {
				dataOutputStream.writeByte(varInt);
				return;
			}
			dataOutputStream.writeByte(varInt & 0x7F | 0x80);
			varInt >>>= 7;
		}
	}
	
	public void writeVarInt(int varInt) throws IOException {
		writeVarInt(this.dataOutputStream, varInt);
	}

	public void writeShort(int v) throws IOException {
		this.dataOutputStream.writeShort(v);
	}

	public void writeByte(int v) throws IOException {
		this.dataOutputStream.writeByte(v);
	}

	public void writeTo(DataOutputStream dataOutStream) throws IOException {
		writeVarInt(dataOutStream, byteArrayOutputStream.size());
		dataOutStream.write(byteArrayOutputStream.toByteArray());
	}

	public void writeLong(long v) throws IOException {
		dataOutputStream.writeLong(v);
	}
	
	public void close() throws IOException {
		this.dataOutputStream.close();
	}
	
	//static read
	
	public static String readString(DataInputStream dataInStream,int length) throws IOException {
		byte[] buffer = new byte[length];
		dataInStream.readFully(buffer);
		return new String(buffer);
	}
	
	public static int readVarInt(DataInputStream dataInStream) throws IOException {
		int result = 0;
		int numRead = 0;
		while (true) {
			int read = dataInStream.readByte();
			result |= (read & 0x7F) << numRead++ * 7;
			if (numRead > 5){
				throw new RuntimeException("var int to big");
			}
			if ((read & 0x80) != 128){
				break;
			}
		}
		return result;
	}
	

}
